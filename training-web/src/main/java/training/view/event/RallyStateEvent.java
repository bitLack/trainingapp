package training.view.event;

import javax.faces.event.ActionEvent;

import training.view.component.RallyCanvas;

@SuppressWarnings("serial")
public class RallyStateEvent extends ActionEvent{

	public RallyStateEvent(RallyCanvas rc) {
		super(rc);
	}
	
	public RallyCanvas getRallyComponent(){
		return (RallyCanvas) getComponent();
	}

}
