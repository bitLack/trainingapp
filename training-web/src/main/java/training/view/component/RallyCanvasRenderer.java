package training.view.component;

import java.io.IOException;

import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.context.ResponseWriter;
import javax.faces.render.FacesRenderer;
import javax.faces.render.Renderer;

@FacesRenderer(componentFamily = "training.view.component", rendererType = "RallyCanvas")
public class RallyCanvasRenderer extends Renderer {
	
	@Override
	public void encodeBegin(FacesContext context, UIComponent component) 
	        throws IOException {
		if(context == null || component == null){
			throw new NullPointerException();
		}
		RallyCanvas rc = (RallyCanvas) component;
		ResponseWriter writer = context.getResponseWriter();
		writer.startElement("RallyCanvas", rc);
		writer.writeAttribute("name", rc.getId(), "id");
	}

	@Override
	public void encodeEnd(final FacesContext context, UIComponent component) throws IOException {
		if(context == null || component == null){
			throw new NullPointerException();
		}
		
		RallyCanvas rc = (RallyCanvas) component;
		ResponseWriter writer = context.getResponseWriter();
		
	/*	writer.startElement("canvas", rc);
        writer.writeAttribute("id", "canvas", null);
        writer.writeAttribute("width", rc.getWidth(), null);
        writer.writeAttribute("height", rc.getHeight(), null);
        writer.writeAttribute("ondragover", "allowDrop(event)", null);
        writer.writeAttribute("ondrop", "drop(event)", null);
		writer.endElement("canvas");*/
		
		writer.startElement("input", rc);
        writer.writeAttribute("type", "hidden", null);
        writer.writeAttribute("id", "hiddenimages", null);
        writer.writeAttribute("value", new org.primefaces.json.JSONArray(rc.getImages(), true).toString(), null);
        
        writer.startElement("converter", rc);
        writer.writeAttribute("converterId", "mJsonConv", null);
        writer.writeAttribute("type", rc.getConvimages(), null);
        writer.endElement("converter");
        
        writer.endElement("input");
        
        writer.startElement("input", rc);
        writer.writeAttribute("type", "hidden", null);
        writer.writeAttribute("id", "hiddenarrows", null);
        writer.writeAttribute("value", new org.primefaces.json.JSONArray(rc.getArrows(), true).toString(), null);
        
        writer.startElement("converter", rc);
        writer.writeAttribute("converterId", "mJsonConv", null);
        writer.writeAttribute("type", rc.getConvarrows(), null);
        writer.endElement("converter");
        
        writer.endElement("input");
        
        writer.startElement("input", rc);
        writer.writeAttribute("type", "hidden", null);
        writer.writeAttribute("id", "hiddengridsnap", null);
        writer.writeAttribute("value", rc.getGridsnap()+"", null);
        writer.endElement("input");
        
        writer.startElement("input", rc);
        writer.writeAttribute("type", "hidden", null);
        writer.writeAttribute("id", "hiddenshowarrows", null);
        writer.writeAttribute("value", rc.getShowarrows()+"", null);
        writer.endElement("input");
        
        writer.startElement("input", rc);
        writer.writeAttribute("type", "hidden", null);
        writer.writeAttribute("id", "hiddenshoworder", null);
        writer.writeAttribute("value", rc.getShoworder()+"", null);
        writer.endElement("input");
        
        writer.startElement("script", component);
        writer.write("load();");
        writer.endElement("script");
		// TODO complete what is needed in the html
		writer.endElement("RallyCanvas");
	}
	
	// --------------------------------------------------------- Private Methods
	
}
