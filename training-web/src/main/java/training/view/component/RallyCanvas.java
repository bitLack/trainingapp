package training.view.component;

import java.util.List;
import java.util.Map;

import javax.faces.component.FacesComponent;
import javax.faces.component.UICommand;
import javax.faces.context.FacesContext;
import javax.faces.event.FacesEvent;

import org.primefaces.util.Constants;

import training.core.entity.CanvasArrow;
import training.core.entity.CanvasImage;
import training.view.event.RallyStateEvent;

@FacesComponent("RallyCanvas")
public class RallyCanvas extends UICommand {

	public static final String COMPONENT_FAMILY = "training.view.component";
//	private static final String DEFAULT_RENDERER = "training.view.component.RallyCanvasRenderer";
	
	public static final String EVENT_STATE_CHANGE = "stateChange";
	
	protected enum PropertyKeys {

		movable,
		resizable,
		maxHeight,
		width,
		height,
		minWidth,
		keyboardSupport,
		images,
		arrows,
		convimages,
		convarrows,
		gridsnap,
		showorder,
		showarrows;

		private String toString;

		PropertyKeys(final String toString) {
			this.toString = toString;
		}

		PropertyKeys() {
		}

		@Override
		public String toString() {
			return ((this.toString != null) ? this.toString : super.toString());
		}
	}
	
	public void setArrows(final List<CanvasArrow> images) {
		getStateHelper().put(PropertyKeys.arrows, images);
	}
	
	@SuppressWarnings("unchecked")
	public List<CanvasArrow> getArrows() {
		//might need to change value here to MethodExpression... 
		return (List<CanvasArrow>) getStateHelper().eval(PropertyKeys.arrows, null);
	}
	
	public void setImages(final List<CanvasImage> images) {
		getStateHelper().put(PropertyKeys.images, images);
	}

	@SuppressWarnings("unchecked")
	public List<CanvasImage> getImages() {
		return (List<CanvasImage>) getStateHelper().eval(PropertyKeys.images, null);
	}
	
	public void setConvimages(final String convimages) {
		getStateHelper().put(PropertyKeys.convimages, convimages);
	}

	public String getConvimages() {
		return (String) getStateHelper().eval(PropertyKeys.convimages, null);
	}
	
	public void setConvarrows(final String convarrows) {
		getStateHelper().put(PropertyKeys.convarrows, convarrows);
	}

	public String getConvarrows() {
		return (String) getStateHelper().eval(PropertyKeys.convarrows, null);
	}
	
	public int getWidth(){
		return (Integer) getStateHelper().eval(PropertyKeys.width, null);
	}
	
	public void setWidth(int width){
		getStateHelper().put(PropertyKeys.width, width);
	}
	
	public int getHeight(){
		return (Integer) getStateHelper().eval(PropertyKeys.height, null);
	}
	
	public void setHeight(int height){
		getStateHelper().put(PropertyKeys.height, height);
	}
	
	public boolean getGridsnap(){
		return (boolean) getStateHelper().eval(PropertyKeys.gridsnap, null);
	}
	
	public void setGridsnap(boolean gs){
		getStateHelper().put(PropertyKeys.gridsnap, gs);
	}
	
	public boolean getShoworder(){
		return (boolean) getStateHelper().eval(PropertyKeys.showorder, null);
	}
	
	public void setShoworder(boolean so){
		getStateHelper().put(PropertyKeys.showorder, so);
	}
	
	public boolean getShowarrows(){
		return (boolean) getStateHelper().eval(PropertyKeys.showarrows, null);
	}
	
	public void setShowarrows(boolean sa){
		getStateHelper().put(PropertyKeys.showarrows, sa);
	}

	@Override
	public String getFamily() {
		return COMPONENT_FAMILY;
	}
	
	@Override
	public void queueEvent(final FacesEvent event) {
		final FacesContext context = FacesContext.getCurrentInstance();
		final Map<String, String> params = context.getExternalContext().getRequestParameterMap();

		final String eventName = params.get(Constants.RequestParams.PARTIAL_BEHAVIOR_EVENT_PARAM);

		if (eventName.equals(EVENT_STATE_CHANGE)){

			final RallyStateEvent changeEvent =
					new RallyStateEvent(this);

			super.queueEvent(changeEvent);
		}
	}

}
