package training.view.ctrl;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.Blogg;
import training.core.entity.Account;
import training.core.entity.PasswordResetToken;


@Named
@RequestScoped
public class PasswordResetCtrl {
	@Inject
	private Blogg blogg; 

	public PasswordResetToken getToken(String token){
		PasswordResetToken pwrt = null;
		try {
			pwrt = blogg.getPasswordResetTokenDAO().getByToken(token);
		} catch (Exception e) {
			// TODO: handle exception
		}
		return pwrt;
	}
	
	public void removeToken(Long id){
		blogg.getPasswordResetTokenDAO().delete(id);
	}
	
	public void changePassword(Account acc){
		blogg.getAccountDAO().update(acc);
	}
}
