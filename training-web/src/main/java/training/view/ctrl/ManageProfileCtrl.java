package training.view.ctrl;

import java.io.Serializable;
import java.util.Calendar;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import training.core.Blogg;
import training.core.entity.Account;

@Named
@RequestScoped
public class ManageProfileCtrl implements Serializable{
	private static final long serialVersionUID = -4667089144114472524L;

	@Inject
    private Blogg blogg;
	
	
	protected ManageProfileCtrl(){}
	
	public String updateAccount(Account ac){
		Calendar cal = Calendar.getInstance();
		ac.setChangeDate(cal.getTime());
		blogg.getAccountDAO().update(ac);
		
		return "Success";
	}
	
	public Account getAccount(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		List<Account> acs = blogg.getAccountDAO().getByEmail(auth.getName());
		return acs.get(0);	// should only be one account per email
	}
}
