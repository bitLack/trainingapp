package training.view.ctrl;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.core.context.SecurityContextHolder;

import training.core.Blogg;
import training.core.entity.Account;
import training.core.entity.Feedback;

@Named
@RequestScoped
public class FeedbackCtrl implements Serializable{
	private static final long serialVersionUID = -6033310399888832852L;
	
	
	@Inject
	private Blogg blogg;
	
	public String storeFeedbacl(String topic, String title, String text){
		String email = SecurityContextHolder.getContext().getAuthentication().getName();
		
		List<Account> users = blogg.getAccountDAO().getByEmail(email); 
		if(users.isEmpty()){
			return "user not found";
		}
		Account user = users.get(0);
		
		Feedback fb = new Feedback(topic, title, text, user);
		try {
			blogg.getFeedbackDAO().create(fb);
		} catch (Exception e) {
			return "failed";
		}
		return "success";
	}
}
