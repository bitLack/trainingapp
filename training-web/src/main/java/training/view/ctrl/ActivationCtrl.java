package training.view.ctrl;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.Blogg;
import training.core.entity.Account;

@Named
@RequestScoped
public class ActivationCtrl implements Serializable{
	private static final long serialVersionUID = 7042153273426459908L;
	
	@Inject
    private Blogg blogg;
	
	public Account getAccount(String key){
		Account acc;
		try {
			acc = blogg.getAccountDAO().geyByActivationKey(key);
		} catch (Exception e) {
			return null;
		}
		
		
		return acc;
	}
	
	public void saveAccount(Account acc){
		blogg.getAccountDAO().update(acc);
	}

}
