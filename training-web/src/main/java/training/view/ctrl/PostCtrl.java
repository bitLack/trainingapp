package training.view.ctrl;

import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import training.core.Blogg;
import training.core.entity.Account;
import training.core.entity.Bloggpost;

@Named
@RequestScoped
public class PostCtrl {

	@Inject
	Blogg blogg;
	
	
	public String createPost(Bloggpost bloggpost){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		List<Account> acs = blogg.getAccountDAO().getByEmail(auth.getName());
		if(!acs.isEmpty()){
			bloggpost.setAuthour(acs.get(0));
		}else{
			//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Not logged in!"));
			return "error";
		}
		blogg.getBloggpostDAO().update(bloggpost);
		return "success";
	}
	
	public ArrayList<Bloggpost> getPosts(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return blogg.getBloggpostDAO().findAuthorized(auth.getName());
	}
}
