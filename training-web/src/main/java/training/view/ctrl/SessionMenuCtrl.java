package training.view.ctrl;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;

@SuppressWarnings("serial")
@Named
@SessionScoped
public class SessionMenuCtrl implements Serializable{

	public String logout(){
		HttpServletRequest request = (HttpServletRequest) (FacesContext.getCurrentInstance().getExternalContext().getRequest());
        HttpServletResponse response = (HttpServletResponse) (FacesContext.getCurrentInstance().getExternalContext().getResponse());
        
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null){    
			  new SecurityContextLogoutHandler().logout(request, response, auth);
		}
	/*	try {
			request.logout();
		} catch (ServletException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		SecurityContextHolder.getContext().setAuthentication(null);
		return "logout";
	}
}
