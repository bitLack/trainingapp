package training.view.ctrl;

import java.io.Serializable;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.Blogg;
import training.core.entity.Account;
import training.security.core.Password;
import training.view.bb.RegisterAccountBB;
import training.view.util.SendEmail;

/**
 *
 * @author Fredrik
 */
@Named
@RequestScoped
public class RegisterAccountCtrl implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Inject
    private Blogg blogg;
	
	@Inject
	private SendEmail sendemail;
	
	private final static Logger logger = Logger.getLogger(RegisterAccountCtrl.class.getName());
    
    protected RegisterAccountCtrl(){
        
    }
    
    @PostConstruct
    public void init(){
    	logger.setLevel(Level.FINE);
    }
    
    public boolean emailExists(String email){
        return !blogg.getAccountDAO().getByEmail(email).isEmpty();
    }
    
    public String registerAccount(RegisterAccountBB data){
        
        Password pwdHash = new Password(data.getPassword());

        Account account = new Account(data.getEmail(), pwdHash.getHash(), data.getName());
        account.setActivationkey(UUID.randomUUID().toString());
        try {
        	blogg.getAccountDAO().create(account);
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Failed to register: " + e.toString());
			return "failed";
		}
        
        sendemail.send(data.getEmail(), data.getName(), account.getActivationkey());
        return "success";
    }

}
