package training.view.ctrl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import javax.ejb.EJBTransactionRolledbackException;
import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import training.core.Blogg;
import training.core.entity.Account;
import training.core.entity.CanvasImage;
import training.core.entity.CanvasMap;
import training.view.bb.RallyStaticBB;

@Named
@SessionScoped
public class CanvasCtrl implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private final static Logger logger = Logger.getLogger(CanvasCtrl.class.getName());

	
	@Inject
	Blogg blogg;
	
	@Inject
	private RallyStaticBB rsbb;
		
	//protected CanvasMap cM;
	
	private ArrayList<CanvasMap> savedMapscache;
	
	private HashMap<Long,CanvasMap> activeCanvasMaps ;
		
	protected CanvasCtrl(){}
	
	@PostConstruct
	protected void init(){
		activeCanvasMaps = new HashMap<>();
		getSavedMaps();
		logger.setLevel(Level.FINE);
		logger.log(Level.INFO, "init CanvasCtrl");
	}
	
	public ArrayList<CanvasMap> loadAll(){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		return new ArrayList<CanvasMap>(blogg.getCanvasMapDAO().findAuthorized(auth.getName()));
	}
	
	@PreDestroy
	public void storeMem(){
		if(activeCanvasMaps != null && activeCanvasMaps.size() > 0){
			logger.log(Level.INFO, "Store memory to DB.");
			// maybe add bulk update here
			for(Entry<Long,CanvasMap> entry : activeCanvasMaps.entrySet()){
				CanvasMap cM = entry.getValue();
				save(cM);
			}
		}
	}
	
	public String saveInMemory(CanvasMap cM){
		logger.log(Level.INFO, "Save canvasmap to memory.");
		activeCanvasMaps.put(cM.getId(), cM);
		return "success";
	}
	
	public CanvasMap storeToDB(CanvasMap cM){
		return blogg.getCanvasMapDAO().update(cM);
	}
	
	public CanvasMap save(CanvasMap cM){
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		List<Account> acs = blogg.getAccountDAO().getByEmail(auth.getName());
		if(!acs.isEmpty()){
			logger.log(Level.FINE, "Set the author to canvasmap to: " +  acs.get(0));
			cM.setAuthour(acs.get(0));
		}else{
			//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error!", "Not logged in!"));
			return cM;
		}
		updateLevel(cM);
		cM.setPublicview("false");
		try {
			logger.log(Level.INFO, "savedMapscache size : " + savedMapscache.size());
			if(checkUniqeName(cM)){
				Calendar cal = Calendar.getInstance();
				cM.setChangeDate(cal.getTime());
				logger.log(Level.INFO, "Store canvasmap to DB: " + cM.toString());
				cM = storeToDB(cM);
			}
		} catch (EJBTransactionRolledbackException e) {
			logger.log(Level.WARNING, "EJBTransactionRolledbackException: " + e.toString(), e);
			return cM;
		} catch (Exception e) {
			logger.log(Level.SEVERE, "Exception: " + e.toString(), e);
			return cM;
		}
		
		return cM;
	}
	
	private void updateLevel(CanvasMap cM){
		String maxLevel = rsbb.getLevels().get(0);
		for(CanvasImage cimg : cM.getCanvasImages()){
			if(cimg.getName().equals("mol") || cimg.getName().equals("start") ){
				continue;
			}
			int name = Integer.parseInt(cimg.getName());
			if(maxLevel.equals(rsbb.getLevels().get(0)) && name < 300 && name > 200){
				maxLevel = rsbb.getLevels().get(1);
			}else if(!maxLevel.equals(rsbb.getLevels().get(3)) && name < 400 && name > 300){
				maxLevel = rsbb.getLevels().get(2);
			} else if(!maxLevel.equals(rsbb.getLevels().get(3)) && name > 400){
				maxLevel = rsbb.getLevels().get(3);
			}
		}
		cM.setLevel(maxLevel);
	}
	
	private boolean checkUniqeName(CanvasMap cM){
		return true;	// not sure if we need this
	/*	for(int i = 0; i < savedMapscache.size(); i++){
			CanvasMap tcM = savedMapscache.get(i);
			logger.log(Level.INFO, "CheckUniqe: Name : " + tcM.getName() + " == " +cM.getName());
			logger.log(Level.INFO, "CheckUniqe: ID : " + tcM.getId() +" != " +cM.getId());
			if(tcM.getName().equals(cM.getName()) && tcM.getId() != cM.getId()){
				logger.log(Level.INFO, "CheckUniqe: return false ");
				return false;
			}
		}
		return true;*/
	}

	public ArrayList<CanvasMap> getSavedMaps() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		setSavedMapscache( blogg.getCanvasMapDAO().findAuthorized(auth.getName()));
		logger.log(Level.INFO, "Fetching stored canvasMaps from DB : " + getSavedMapscache().toString());
		return getSavedMapscache();
	}

	public String remove(CanvasMap canvasMap) {
		blogg.getCanvasMapDAO().delete(canvasMap.getId());
		activeCanvasMaps.remove(canvasMap.getId());
		savedMapscache.remove(canvasMap);
		return "removed";
	}

	public ArrayList<CanvasMap> getSavedMapscache() {
		return this.savedMapscache;
	}
	
	public void setSavedMapscache(ArrayList<CanvasMap> savedMapscache) {
		this.savedMapscache = savedMapscache;
	}

	/**
	 * @return the activeCanvasMaps
	 */
	public HashMap<Long, CanvasMap> getActiveCanvasMaps() {
		return activeCanvasMaps;
	}

	/**
	 * @param activeCanvasMaps the activeCanvasMaps to set
	 */
	public void setActiveCanvasMaps(HashMap<Long, CanvasMap> activeCanvasMaps) {
		this.activeCanvasMaps = activeCanvasMaps;
	}

}
