package training.view.ctrl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import training.core.Blogg;
import training.core.entity.Account;
import training.core.entity.PasswordResetToken;
import training.view.util.SendEmail;
/**
 *
 * @author Fredrik
 * 
 */
@Named
@RequestScoped
public class LoginAccountCtrl implements Serializable{
    
	private final static Logger logger = Logger.getLogger(LoginAccountCtrl.class.getName());
	private static final long serialVersionUID = 1L;
	
	@Inject
	private Blogg blogg;
	
	@Inject
	private SendEmail sendemail;

	protected LoginAccountCtrl(){      
    }
	
	@PostConstruct
	public void init(){
		logger.setLevel(Level.INFO);
	}
    
    public void doLogin(String email, String password) throws ServletException, IOException{
        ExternalContext context = FacesContext.getCurrentInstance()
                .getExternalContext();
        
        RequestDispatcher dispatcher = ((ServletRequest) context.getRequest())
                .getRequestDispatcher("/j_spring_security_check?j_username=" + email
                                + "&j_password=" + password);
        
        // Forwards to original destination or to error page
        dispatcher.forward((ServletRequest) context.getRequest(),
                (ServletResponse) context.getResponse());
        FacesContext.getCurrentInstance().responseComplete();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if(!(auth instanceof AnonymousAuthenticationToken)){
        	updateLogins(auth.getName());
        }
    }
    
    private void updateLogins(String email){
		try {
			List<Account> accs = blogg.getAccountDAO().getByEmail(email);
			if(accs == null || accs.isEmpty()){
				logger.log(Level.WARNING, "No account found " + email );
				return;
			}
			Account acc = accs.get(0);
			acc.setLatestLoginDate(Calendar.getInstance().getTime());
			acc.setNumberLogins(acc.getNumberLogins()+1);
			blogg.getAccountDAO().update(acc);
		} catch (Exception e) {
			logger.log(Level.WARNING, "Something went wrong when updating account " + email + " Error: " + e.toString());
			e.printStackTrace();
		}
	}
    
    public void sendpasswordResetEmail(String email){
    	List<Account> accs = blogg.getAccountDAO().getByEmail(email);
    	System.out.println("accounts found: " + accs.toString());
    	if(accs == null || accs.isEmpty()){
    		return;
    	}
    	PasswordResetToken prtoken = new PasswordResetToken(accs.get(0));
    	sendemail.sendPasswordReset(email, prtoken.getToken());
    	
    	blogg.getPasswordResetTokenDAO().create(prtoken);
    }

}
