package training.view.bb;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.servlet.ServletException;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import training.view.ctrl.LoginAccountCtrl;
/**
 *
 * @author Snjezana & Hampus
 */
@Named
@RequestScoped
public class LoginAccountBB implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@NotNull(message = "You need to enter your email!")
    @Pattern(message = "Invalid email format" ,
            regexp = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
        +"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
        +"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?")
    private String email;
    @NotNull(message = "Password is needed!")
    private String password;
    
    private LoginAccountCtrl loginCtrl;

    
    protected LoginAccountBB(){}

    @Inject
    public LoginAccountBB(LoginAccountCtrl loginCtrl) {
        this.loginCtrl = loginCtrl;
    }
    
    public void doLogin() throws IOException, ServletException {
        loginCtrl.doLogin(email, password);
    }
    
    public void sendPasswordReset(){
    	System.out.println("sendpassword");
    	loginCtrl.sendpasswordResetEmail(email);
    	FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Återställnings email har skickats till "+email,"");
        FacesContext.getCurrentInstance().addMessage(null, msg);
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    
}
