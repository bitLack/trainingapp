package training.view.bb;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;

import training.view.ctrl.SessionMenuCtrl;

@SuppressWarnings("serial")
@Named
@SessionScoped
public class SessionMenuBB implements Serializable{

	private SessionMenuCtrl sMC;
	
	protected SessionMenuBB(){}
	
	@Inject
	protected SessionMenuBB(SessionMenuCtrl sessionMenuCtrl){
		sMC = sessionMenuCtrl;
	}
	
	public String logout(){
		return sMC.logout();
	}
	
	public boolean isLoggedIn() {
        return !(SecurityContextHolder.getContext().getAuthentication() instanceof AnonymousAuthenticationToken);
    }
}
