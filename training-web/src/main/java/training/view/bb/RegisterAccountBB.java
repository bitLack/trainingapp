package training.view.bb;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

import training.view.ctrl.RegisterAccountCtrl;
import training.view.util.ValidateEmail;

/**
 *
 * @author Strom
 */
@Named
@RequestScoped
public class RegisterAccountBB implements Serializable{
    /**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String regex = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
	        +"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
	        +"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
	
	@NotNull(message = "You need to enter your email!")
    @ValidateEmail(message = "Email already registerd")
    private String email;
    @NotNull(message = "Password is needed!")
    @Size(min = 4, max = 255, message = "Password needs to have 4-255 characters")
    private String password;
    @NotNull(message = "Name is needed")
    @Size(min = 3, max = 16, message = "Name needs to have 3-16 characters")
    private String name;
    
    private RegisterAccountCtrl regCrtl;
    
    protected RegisterAccountBB(){
        
    }

    @Inject
    public RegisterAccountBB(RegisterAccountCtrl regCrtl) {
        this.regCrtl = regCrtl;
    }
    
    public String registerAccount(){
    	String result = regCrtl.registerAccount(this);
    	if(result.equals("failed")){
    		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_WARN, "Email already exists!", "Email already exists!");
            FacesContext.getCurrentInstance().addMessage(null, msg);
    		return result;
    	}
        FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correct", "Correct");
        FacesContext.getCurrentInstance().addMessage(null, msg);
        return result;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

	/**
	 * @return the regex
	 */
	public String getRegex() {
		return regex;
	}

	/**
	 * @param regex the regex to set
	 */
	public void setRegex(String regex) {
		this.regex = regex;
	}
    
    
    
}
