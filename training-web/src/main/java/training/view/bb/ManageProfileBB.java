package training.view.bb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.entity.Account;
import training.view.ctrl.ManageProfileCtrl;

@Named
@RequestScoped
public class ManageProfileBB implements Serializable{
	private static final long serialVersionUID = 4520136029157153705L;

	private Account myaccount;
	
	@Inject
	private ManageProfileCtrl mpc;
	
	
	protected ManageProfileBB(){}
	
	@PostConstruct
	private void init(){
		myaccount = mpc.getAccount();
	}

	/**
	 * @return the myaccount
	 */
	public Account getMyaccount() {
		return myaccount;
	}


	/**
	 * @param myaccount the myaccount to set
	 */
	public void setMyaccount(Account myaccount) {
		this.myaccount = myaccount;
		mpc.updateAccount(myaccount);
	}
	
	
}
