package training.view.bb;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.entity.Account;
import training.view.ctrl.ActivationCtrl;


@Named
@RequestScoped
public class ActivationBB implements Serializable{
	private static final long serialVersionUID = -2778695532125276799L;
	
	@Inject
	private ActivationCtrl ac;

	private String key;
	
	private boolean valid;
	
	@PostConstruct
	public void init(){
		key = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap().get("key");
		valid = checkKey();
		
	}

	private boolean checkKey() {
		if(key == null || key.isEmpty()){
			return false;
		}
		Account acc = ac.getAccount(key);
		if(acc != null){
			acc.setActivationkey(null);
			acc.setEnabled(1);
			ac.saveAccount(acc);
			return true;
		}
		return false;
	}

	/**
	 * @return the valid
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * @param valid the valid to set
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
	}
}
