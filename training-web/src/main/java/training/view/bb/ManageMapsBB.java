package training.view.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.entity.CanvasMap;
import training.view.ctrl.CanvasCtrl;

@Named
@RequestScoped
public class ManageMapsBB implements Serializable{
	private static final long serialVersionUID = -30363210597997875L;
	
	private final static Logger logger = Logger.getLogger(ManageMapsBB.class.getName());

	@Inject
	private CanvasCtrl cc;
	
	private ArrayList<CanvasMap> savedCM;
	
	private CanvasMap selectedCanvasMap;
	
	protected ManageMapsBB(){}
	
	@PostConstruct
	public void init(){
		logger.setLevel(Level.FINE);
		logger.log(Level.INFO,"init ManageMapsBB");
		cc.storeMem();
		savedCM = cc.getSavedMaps();
	}
	
	public String newRallyMap(){
		logger.log(Level.INFO,"new rallymap");

		FacesContext.getCurrentInstance().getExternalContext().
				getFlash().put("rallyid", "new");
		return "rallyeditor";
	}
	
	public String openRallyMap(){
		logger.log(Level.INFO,"open rallymap: " +selectedCanvasMap);
		if(selectedCanvasMap == null){
			return null;
		}
		cc.saveInMemory(selectedCanvasMap);
		FacesContext.getCurrentInstance().getExternalContext().
				getFlash().put("rallyid", ""+selectedCanvasMap.getId());
		return "rallyeditor";
	}
	
	public void deleteRallyMap(){
		logger.log(Level.INFO,"delete rallymap: " +selectedCanvasMap);
		cc.remove(selectedCanvasMap);
	}

	/**
	 * @return the savedCM
	 */
	public ArrayList<CanvasMap> getSavedCM() {
		return savedCM;
	}

	/**
	 * @param savedCM the savedCM to set
	 */
	public void setSavedCM(ArrayList<CanvasMap> savedCM) {
		this.savedCM = savedCM;
	}

	/**
	 * @return the selectedCanvasMap
	 */
	public CanvasMap getSelectedCanvasMap() {
		return selectedCanvasMap;
	}

	/**
	 * @param selectedCanvasMap the selectedCanvasMap to set
	 */
	public void setSelectedCanvasMap(CanvasMap selectedCanvasMap) {
		this.selectedCanvasMap = selectedCanvasMap;
	}
	
}
