package training.view.bb;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;


@Named
@SessionScoped
public class RallyStaticBB implements Serializable{
	private static final long serialVersionUID = -6764006669274052748L;

	private final static Logger logger = Logger.getLogger(RallyStaticBB.class.getName());

	private ArrayList<String> levels;
	
	private Map<String, ArrayList<String>> imgdata;

	protected RallyStaticBB() {}
	
	
	@PostConstruct
	public void init(){
		logger.setLevel(Level.FINE);
		logger.log(Level.INFO, "init RallyStaticBB");
		
		imgdata = new HashMap<String, ArrayList<String>>();
		
		levels = new ArrayList<String>();
		levels.add("Nybörjarklass");
		levels.add("Fortsättningsklass");
		levels.add("Avanceradklass");
		levels.add("Mästarklass");
		
		int[] maximgs = {131, 219, 315, 414};
		
		ArrayList<String> imgs;
		for(int k = 0; k < levels.size(); k++){
			imgs = new ArrayList<String>();
			if(k==0){
				imgs.add("start");
				imgs.add("mol");
			}
			int i = 100*(k+1)+1;
			for(; i < maximgs[k]; i++){
				imgs.add(""+i);
			}
			imgdata.put(levels.get(k), imgs);
		}
	}
	
	public String getSelectedImgSrc(String klass){
		if(klass.equals(levels.get(0))){
			return "nyborjarklass";
		}else if(klass.equals(levels.get(1))){
			return "fortsattningsklass";
		}else if(klass.equals(levels.get(2))){
			return "avanceradklass";
		}else if(klass.equals(levels.get(3))){
			return "mastarklass";
		}
		return "";
	}

	public String getDefaultName(){
		return "Namnlös";
	/*	String newname = "Ny rallybana " + cc.getSavedMapscache().size();
		ArrayList<CanvasMap> cache = cc.getSavedMapscache();
		for(int i = 0; i < cache.size(); i ++){
			if(cache.get(i).getName() == newname){
				newname += ""+ cc.getSavedMapscache().size();
				i = 0;
			}
		}
		return newname;*/
	}

	public ArrayList<String> getImgs(String klass) {
		if(klass.equals(levels.get(0))){
			return imgdata.get(levels.get(0));
		}else if(klass.equals(levels.get(1))){
			return imgdata.get(levels.get(1));
		}else if(klass.equals(levels.get(2))){
			return imgdata.get(levels.get(2));
		}else if(klass.equals(levels.get(3))){
			return imgdata.get(levels.get(3));
		}
		return new ArrayList<String>();
	}

	public ArrayList<String> getLevels() {
		return levels;
	}
	
	public void setLevels(ArrayList<String> levels) {
		this.levels = levels;
	}
	
}
