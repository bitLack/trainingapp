package training.view.bb;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.inject.Named;

import training.view.ctrl.FeedbackCtrl;

@RequestScoped
@Named
public class FeedbackBB implements Serializable{
	private static final long serialVersionUID = 4328907588311352361L;

	@Inject
	private FeedbackCtrl fc;
	
	private String text;
	private String title;
	private String topic;
	
	
	public void storeFeedback(){
		String result = fc.storeFeedbacl(topic, title, text);
		if(result.equals("success")){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Din åsikt har tagits emot."));
		}else{
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Något gick fel, försök igen."));
		}
	}

	/**
	 * @return the text
	 */
	public String getText() {
		return text;
	}

	/**
	 * @param text the text to set
	 */
	public void setText(String text) {
		this.text = text;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the topic
	 */
	public String getTopic() {
		return topic;
	}

	/**
	 * @param topic the topic to set
	 */
	public void setTopic(String topic) {
		this.topic = topic;
	}
	
}
