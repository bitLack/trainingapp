package training.view.bb;

import java.io.Serializable;
import java.util.Calendar;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.entity.Account;
import training.core.entity.PasswordResetToken;
import training.security.core.Password;
import training.view.ctrl.PasswordResetCtrl;

@Named
@ViewScoped
public class PasswordResetBB implements Serializable{
	private static final long serialVersionUID = -6935684369588233340L;

	@Inject
	private PasswordResetCtrl prc;
	
	private String token;
	private boolean valid;
	private String newpassword;
	
	private PasswordResetToken prtoken;
	
	@PostConstruct
	public void init(){
		token = FacesContext.getCurrentInstance().
                getExternalContext().getRequestParameterMap().get("token");
		prtoken = prc.getToken(token);
		valid = checkToken();
		return;
	}
	
	private boolean checkToken() {
		if(prtoken != null){
			if(prtoken.getExpiryDate().getTime() - Calendar.getInstance().getTime().getTime() <= 0 ){
				return false;
			}
			prc.removeToken(prtoken.getId());
			return true;
		}
		return false;
	}
	
	public String registernewpassword(){
		Password pwd = new Password(newpassword);
		Account acc = prtoken.getAcc();
		acc.setPassword(pwd.getHash());
		prc.changePassword(acc);
		return "success";
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
		return;
	}

	/**
	 * @return the valid
	 */
	public boolean isValid() {
		return valid;
	}

	/**
	 * @param valid the valid to set
	 */
	public void setValid(boolean valid) {
		this.valid = valid;
		return;
	}

	/**
	 * @return the newpassoword
	 */
	public String getNewpassword() {
		return newpassword;
	}

	/**
	 * @param newpassoword the newpassoword to set
	 */
	public void setNewpassword(String newpassword) {
		this.newpassword = newpassword;
		return;
	}

}
