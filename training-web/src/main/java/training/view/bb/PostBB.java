package training.view.bb;

import java.io.Serializable;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.entity.Bloggpost;
import training.view.ctrl.PostCtrl;



@SuppressWarnings("serial")
@Named
@RequestScoped
public class PostBB implements Serializable{
	private String text;
	
	private PostCtrl pC;
	
	protected PostBB(){}
	
	@Inject
	public PostBB(PostCtrl pC){
		this.pC = pC;
	}
	
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	
	public String submit(){
		Bloggpost bp = new Bloggpost();
		bp.setContent(text);
		
		return pC.createPost(bp);		
	}
	
}
