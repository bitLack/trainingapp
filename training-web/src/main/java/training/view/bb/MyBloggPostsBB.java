package training.view.bb;

import java.io.Serializable;
import java.util.ArrayList;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import training.core.entity.Bloggpost;
import training.view.ctrl.PostCtrl;

@SuppressWarnings("serial")
@Named
@RequestScoped
public class MyBloggPostsBB implements Serializable{
	private ArrayList<Bloggpost> bloggPosts;
	//private PostCtrl pC;
	
	protected MyBloggPostsBB(){}
	
	@Inject
	public MyBloggPostsBB(PostCtrl pC){
		//this.pC = pC;
		this.setBloggPosts(pC.getPosts());
	}

	public ArrayList<Bloggpost> getBloggPosts() {
		return bloggPosts;
	}

	public void setBloggPosts(ArrayList<Bloggpost> bloggPosts) {
		this.bloggPosts = bloggPosts;
	}
	

}
