package training.view.bb;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.context.RequestContext;

import training.core.entity.CanvasArrow;
import training.core.entity.CanvasImage;
import training.core.entity.CanvasMap;
import training.view.ctrl.CanvasCtrl;

@Named
@ViewScoped
public class CanvasBB implements Serializable{
	private static final long serialVersionUID = 1L;
	
	private final static Logger logger = Logger.getLogger(CanvasBB.class.getName());
		
	private CanvasMap canvasMap;
	private String name; 
	private String selectedKlass = "Nybörjarklass";
	
	private int width, height;
	
	private boolean cm_gridsnap;
	private boolean cm_showorder;
	private boolean cm_showarrows;
	
	@Inject
	private RallyStaticBB rsbb;
		
	@Inject
	private CanvasCtrl cc;
		
	public String getTypeCanvasImageGenericList() {  
        return "java.util.ArrayList<training.core.entity.CanvasImage>";  
    }  
	
	public String getTypeCanvasArrowGenericList(){
		return "java.util.ArrayList<training.core.entity.CanvasArrow>";  
	}
	
	public CanvasBB(){
		logger.setLevel(Level.FINE);
		logger.log(Level.INFO, "construct CanvasBB");
		return;
	}
	
	@PostConstruct
	public void init(){
	/*	try {
			fileHandler = new FileHandler("RallyBBLogg.log", 1000000, 10, true);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		if(fileHandler != null){
			fileHandler.setFormatter(new SimpleFormatter());
			//logger.addHandler(fileHandler);
		}*/
		logger.setLevel(Level.FINE);
		logger.log(Level.INFO, "init CanvasBB");
		
		cc.storeMem();
		cc.getSavedMaps();
		// create a new instance
		FacesContext context = FacesContext.getCurrentInstance();
		String strID = (String) context.getExternalContext().getFlash().get("rallyid");
		if(strID == null){
			logger.log(Level.SEVERE, "No request parameter! ");
			try {
				FacesContext.getCurrentInstance().getExternalContext()
				.redirect("manageMaps.xhtml");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return ;
		}else if(strID == "new"){
			newRallyMap();
			context.getExternalContext().getFlash().put("rallyid", canvasMap.getId());
		}else{
			Long rallyid = (Long) Long.parseLong(strID);
			canvasMap = cc.getActiveCanvasMaps().get(rallyid);
			if(canvasMap == null){
				logger.log(Level.WARNING, "Rallymap not saved in session! " + rallyid);
				return ; 
			}else{
				
				loadDataMap();
				logger.log(Level.INFO, "Opened rallymap: id " + canvasMap.getId());
			}
		}
		updateAttributes();
		
		
		//show helping dialog
		//RequestContext.getCurrentInstance().execute("PF('dlg1').show();");
		
		return ;
	}
	
	private void newRallyMap(){
		canvasMap = new CanvasMap();
		canvasMap.setCanvasImages(new ArrayList<CanvasImage>());
		canvasMap.setCanvasArrows(new ArrayList<CanvasArrow>());
		canvasMap.setName(getDefaultName());
		canvasMap.setLevel("Nybörjarklass");
		canvasMap.setPublicview("false");
		canvasMap = cc.save(canvasMap);
		cc.saveInMemory(canvasMap);
		return;
	}
	
	private void updateAttributes(){
		name = canvasMap.getName();
		
		width = canvasMap.getWidth();
		height = canvasMap.getHeight();
		cm_gridsnap = canvasMap.getGridsnap();
		System.out.println("gridsnap:" + cm_gridsnap);
		cm_showorder = canvasMap.getShowOrd();
		cm_showarrows = canvasMap.getShowArrows();
		return;
	}
	
	private String getDefaultName(){
		return "Namnlös";
	}
	
	public void resizeCanvas(){
		System.out.println("resizeCanvas");
		canvasMap.setWidth(width);
		canvasMap.setHeight(height);
		canvasMap.setGridsnap(cm_gridsnap);
		canvasMap.setShowOrd(cm_showorder);
		canvasMap.setShowArrows(cm_showarrows);
		return ;
	}
	
	
	public void saveCanvas(final ArrayList<CanvasImage> imageX, final ArrayList<CanvasArrow> arrowX){
		if(imageX.isEmpty()){
			logger.log(Level.WARNING, "Not saving empty canvasMap.");
			return;
		}
		if(canvasMap.getName() == null ){
			canvasMap.setName(getDefaultName());
		}else if(canvasMap.getName().isEmpty()){
			canvasMap.setName(getDefaultName());
		}
		if(canvasMap.getLevel().isEmpty()){
			canvasMap.setLevel("Nybörjarklass");
		}
		canvasMap.setCanvasImages(imageX);
		canvasMap.setCanvasArrows(arrowX);
		cc.saveInMemory(canvasMap);
		logger.log(Level.INFO, "Saved canvasMap in memory: " + canvasMap.toString());
		return ;
	}
	
	public void loadDataMap(){
		CanvasMap result = cc.save(canvasMap);
		if(result != null){
			getSavedMaps();
			//FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Saved " + canvasMap.getName() + " successfully!"));
		}else if(result == null){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, "Error", "Save " + canvasMap.getName() + " Failed!"));
		}
		
		RequestContext context = RequestContext.getCurrentInstance();
		if(canvasMap == null ){
			context.addCallbackParam("saved", false); 
			return ;
		}else 
		if(canvasMap.getCanvasImages().isEmpty()){
			context.addCallbackParam("saved", false); 
			return ;
		}
		context.addCallbackParam("gridsnap", canvasMap.getGridsnap().toString());
		context.addCallbackParam("showorder", canvasMap.getShowOrd());
		context.addCallbackParam("showarrows", canvasMap.getShowArrows());
		
		context.addCallbackParam("saved", true);    //basic parameter
		
		context.addCallbackParam("images", new org.primefaces.json.JSONArray(canvasMap.getCanvasImages(), true).toString());     //pojo as json
		context.addCallbackParam("arrows", new org.primefaces.json.JSONArray(canvasMap.getCanvasArrows(), true).toString());
		name = canvasMap.getName();
		return ;
	}
	
	public void newCM(){
		logger.log(Level.INFO, "newCM");
		newRallyMap();
		updateAttributes();
		return;
	}
	
	public void removeCM(){
		
		String result = cc.remove(canvasMap);
		if(result == "removed"){
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Info", "Removed " + canvasMap.getName() + " successfully!"));
		}
		logger.log(Level.INFO, "canvasMap removed: " + canvasMap.toString());
		canvasMap = null;
		init();
		return;
	}
	
	public boolean renderKlass(String klass){
		return klass.equals(selectedKlass);
	}

	public ArrayList<String> getImgs() {
		return rsbb.getImgs(selectedKlass);
	}

	public void setImgs(ArrayList<String> imgs) {
		return;
	}
	
	public ArrayList<String> getImgsByName(String klass) {
		return rsbb.getImgs(klass);
	}

	public ArrayList<String> getLevels() {
		return rsbb.getLevels();
	}
	
	public void setLevels(ArrayList<String> levels) {
		rsbb.setLevels(levels);
		return;
	}

	public CanvasMap getCanvasMap() {
		return canvasMap;
	}

	public void setCanvasMap(CanvasMap canvasMap) {
		this.canvasMap = canvasMap;
		return;
	}

	public ArrayList<CanvasMap> getSavedMaps() {
		return cc.getSavedMapscache();
	}

	public void setSavedMaps(ArrayList<CanvasMap> savedMaps) {
		cc.setSavedMapscache(savedMaps);
		return;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
		if(name != null && name.length() > 0 && name != this.canvasMap.getName()){
			logger.log(Level.INFO, "Set canvas name: " + name);
			this.canvasMap.setName(name);
			cc.saveInMemory(canvasMap);
		}
		return;
	}

	/**
	 * @return the selectedKlass
	 */
	public String getSelectedKlass() {
		return selectedKlass;
	}

	/**
	 * @param selectedKlass the selectedKlass to set
	 */
	public void setSelectedKlass(String selectedKlass) {
		this.selectedKlass = selectedKlass;
		return;
	}
	
	public String getSelectedKlassSrc() {
		return rsbb.getSelectedImgSrc(selectedKlass);
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
		return;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
		return;
	}

	/**
	 * @return the cm_gridsnap
	 */
	public boolean isCm_gridsnap() {
		return cm_gridsnap;
	}

	/**
	 * @param cm_gridsnap the cm_gridsnap to set
	 */
	public void setCm_gridsnap(boolean cm_gridsnap) {
		this.cm_gridsnap = cm_gridsnap;
		return;
	}

	/**
	 * @return the cm_showorder
	 */
	public boolean isCm_showorder() {
		return cm_showorder;
	}

	/**
	 * @param cm_showorder the cm_showorder to set
	 */
	public void setCm_showorder(boolean cm_showorder) {
		this.cm_showorder = cm_showorder;
		return;
	}

	/**
	 * @return the cm_showarrows
	 */
	public boolean isCm_showarrows() {
		return cm_showarrows;
	}

	/**
	 * @param cm_showarrows the cm_showarrows to set
	 */
	public void setCm_showarrows(boolean cm_showarrows) {
		this.cm_showarrows = cm_showarrows;
		return;
	}
	
}
