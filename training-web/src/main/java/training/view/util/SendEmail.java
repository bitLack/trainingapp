package training.view.util;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.*;
import javax.mail.internet.*;

@Stateless
@LocalBean
public class SendEmail {
	
	@Resource(mappedName="java:jboss/mail/Gmail")
	Session gmailSession;
	
	public SendEmail(){}
	
	
	@Asynchronous
	public void send(String email, String name, String activationKey){
		String to = "fredrik.strom.lack@gmail.com";
		
		try {
			MimeMessage message = new MimeMessage(gmailSession);
						
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			
			message.setSubject("Jojosen.me Aktiverings mail");
			
			message.setText(name + ", Välkommen till Jojosen.me! \n\nDetta är ett mail skickas "
					+ "automatiskt till dig för att slutföra registrerningen och "
					+ "aktivering av ditt konto hos Jososen.me.\n"
					+ "Klicka på länken nedan för att aktivera ditt konto: \n"
					+ "testas just nu!!! https://jojosen.me/partials/security/activation.xhtml?key="+activationKey+" \n"
					+ "Detta mail går inte att svara på. \n"
					+ "skickat till: " + email);
			
			Transport.send(message);
			System.out.println("Sent mail to " + name +" with email: "+email);
			// notification to me.
			message = new MimeMessage(gmailSession);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject("Jojosen.me Aktiverings mail");
			message.setText(name + " har registrerats sig med email: "+ email);
			Transport.send(message);
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}

	}
	
	@Asynchronous
	public void sendPasswordReset(String email, String token){
		String to = "fredrik.strom.lack@gmail.com";
		
		try {
			MimeMessage message = new MimeMessage(gmailSession);
						
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(email));
			
			message.setSubject("Jojosen.me Återställning av lösenord");
			
			message.setText("Klicka på länken nedan för att återställa ditt lösenord: \n"
					+ "https://jojosen.me/partials/security/passwordReset.xhtml?token="+token+" \n"
					+ "Detta mail går inte att svara på. \n"
					+ "skickat till: " + email);
			
			Transport.send(message);
			System.out.println("Sent mail to email: "+email);
			// notification to me.
			message = new MimeMessage(gmailSession);
			message.addRecipient(Message.RecipientType.TO, new InternetAddress(to));
			message.setSubject("Jojosen.me Återställning av lösenord");
			message.setText(" har återställt sitt lösenord: "+ email);
			Transport.send(message);
			
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}

}
