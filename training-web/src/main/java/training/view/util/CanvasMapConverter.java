package training.view.util;

import java.util.ArrayList;

import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import javax.inject.Inject;

import training.core.entity.CanvasMap;
import training.view.ctrl.CanvasCtrl;

@FacesConverter("canvasMapConverter")
public class CanvasMapConverter implements Converter{
	
	@Inject
	private CanvasCtrl cc;

	@Override
	public Object getAsObject(FacesContext fc, UIComponent uic, String value) {
		if(value != null && value.trim().length() > 0){
			try {
				//CanvasBB cBB =(CanvasBB) fc.getExternalContext().getSessionMap().get("CanvasBB");
				return getById( Integer.parseInt(value));
			} catch (NumberFormatException e) {
				throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valit CanvasMap"));
			}
		}
		return null;
	}
	
	private CanvasMap getById( long id){

		ArrayList<CanvasMap> cms = cc.getSavedMapscache();
		if(cms == null ){
			return null;
		}
		for(CanvasMap cm : cms){
			if(cm.getId() == id){
				return cm;
			}
		}
		return null;
	}

	@Override
	public String getAsString(FacesContext fc, UIComponent uic, Object object) {
		if(object != null && object instanceof CanvasMap){
			return String.valueOf(((CanvasMap)object).getId());
		}
		return null;
	}

}
