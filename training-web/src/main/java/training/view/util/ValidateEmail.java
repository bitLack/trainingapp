package training.view.util;

import static java.lang.annotation.ElementType.*;
import static java.lang.annotation.RetentionPolicy.*;

import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.Payload;


import training.core.Blogg;
import training.view.util.ValidateEmail.ValidateEmailValidator;
/**
 *
 * @author Fredrik
 */
@Target( { METHOD, FIELD, ANNOTATION_TYPE })
@Retention(RUNTIME)
@Constraint(validatedBy = ValidateEmailValidator.class)
@Documented
public @interface ValidateEmail {
    
    
    
    String message() default "{se.chalmers.webapp.util.annotaitions.validateemail}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};

    public class ValidateEmailValidator implements ConstraintValidator<ValidateEmail, String>{
        @Inject
        Blogg blogg;
        
        private final String EMAIL_PATTERN = "[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\."
                +"[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@"
                +"(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?";
        
        private Pattern pattern;
        
        public ValidateEmailValidator() {
        }

        @Override
        public void initialize(ValidateEmail constraintAnnotation) {
            pattern = Pattern.compile(EMAIL_PATTERN);
        }

        @Override
        public boolean isValid(String value, ConstraintValidatorContext context) {
        	boolean exists = false;
        	if(value== null){
        		return false;
        	}
        	try {
        		exists = !blogg.getAccountDAO().getByEmail(value).isEmpty();
			} catch (Exception e) {
				// not sure
			}
        	return (!exists );
        }
    }
       
}
