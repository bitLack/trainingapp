package training.security.core;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

public final class Password {
	private final String hash;
    
    public Password(String password) {
        hash = bCryptEncoder(password);
    }
    
    public String bCryptEncoder(String pwd) {

        BCryptPasswordEncoder bCPassword = new BCryptPasswordEncoder();
        return bCPassword.encode(pwd);
    }
    public String getHash() {
        return hash;
    }
}
