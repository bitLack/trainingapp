(function() {
          var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame ||
                                      window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;
          window.requestAnimationFrame = requestAnimationFrame;
        })();

var imagesOnCanvas = [];
var arrowsOnCanvas = [];
var id = 0;
var gridsnap = true;
var showorder = true;
var showarrows = true;


function load(){
	iOnCanvas = document.getElementById("hiddenimages").value;
	aOnCanvas = document.getElementById("hiddenarrows").value;
	gs = document.getElementById("hiddengridsnap").value;
	so = document.getElementById("hiddengridsnap").value;
	sa = document.getElementById("hiddengridsnap").value;

	gridsnap = eval('(' + gs + ')');
	showorder = eval('(' + so + ')');
	showarrows = eval('(' + sa + ')');
	
	loadData(null,null,{images:iOnCanvas, arrows:aOnCanvas, saved:true});
	var canvas = document.getElementById('canvas');
	if(!canvas ){
    	return;
    }
}

function loadsettings(){
	gs = document.getElementById("hiddengridsnap").value;
	so = document.getElementById("hiddengridsnap").value;
	sa = document.getElementById("hiddengridsnap").value;

	gridsnap = eval('(' + gs + ')');
	showorder = eval('(' + so + ')');
	showarrows = eval('(' + sa + ')');
}

function resize(){
	loadsettings();
	// find better way...
	var id = PF('widthtext').id;
	var w = document.getElementById(id).value;
	id = PF('heighttext').id;
	var h = document.getElementById(id).value;
	
	var gs = PF('gscbox').isChecked();
	var so = PF('socbox').isChecked();
	var sa = PF('sacbox').isChecked();
	var canvas = document.getElementById('canvas');
	if(!canvas || !w || !h ){
    	return;
    }
	canvas.width = w;
	canvas.height = h;
	gridsnap = gs;
	showorder = so;
	showarrows = sa;
	//call backingbean
	resizeCmd(gs,so,sa);
};

function getId(){
	return id;
}

function setId(newid){
	id = newid;
}

function renderScene() {
    requestAnimationFrame(renderScene);

    var canvas = document.getElementById('canvas');
    if(!canvas){
    	return;
    }
    var ctx = canvas.getContext('2d');
    ctx.clearRect(0,0,
        canvas.width,
        canvas.height
    );
    ctx.beginPath();
    ctx.lineWidth = 1;
    ctx.fillStyle = "#fff";
    ctx.fillRect(0,0,canvas.width,canvas.height);
    
    /* vertical lines */
    for (var x = 0; x <= canvas.width; x += 50) {
    	ctx.moveTo(0.5 + x, 0);
    	ctx.lineTo(0.5 + x, canvas.height);
    }
    /* last line*/
    ctx.moveTo(canvas.width, 0);
	ctx.lineTo(canvas.width, canvas.height);
    
    /* horizontal lines */
    for (var y = 0; y <= canvas.height; y += 50) {
    	ctx.moveTo(0, 0.5 + y);
    	ctx.lineTo(canvas.width, 0.5 +  y);
    }
    /* last line*/
    ctx.moveTo(0, canvas.height);
	ctx.lineTo(canvas.width, canvas.height);
    
    ctx.strokeStyle = "#ccc";
    
    ctx.stroke();
    ctx.closePath();
    
    // draw Arrow
    if(showarrows){
	    for(var x = 0,len = arrowsOnCanvas.length; x < len; x++) {
	    	var arrow = arrowsOnCanvas[x];
	        drawArrow(ctx, arrow.x, arrow.y, arrow.tox, arrow.toy);
	    }
    }
    // draw image
    var hovering = false;
    for(var x = 0,len = imagesOnCanvas.length; x < len; x++) {
        var obj = imagesOnCanvas[x];
        if(isOutSideCanvas(obj,canvas)){
        	ctx.beginPath();
        	ctx.lineWidth = 6;
            ctx.strokeStyle = "red";
        	ctx.rect(obj.x-2,obj.y-2,obj.width+4,obj.height +4);
        	
        	ctx.stroke();
        	ctx.closePath();
        }
        
        /* rotate image*/
    	ctx.save();                
    	ctx.translate(obj.x+(obj.width/2),obj.y+(obj.height/2));
    	ctx.rotate(obj.angle);
    	if(obj.angle == 0 || obj.angle == Math.PI){
    		ctx.drawImage(obj.image,-obj.width/2,-obj.height/2);
    	}else{
    		ctx.drawImage(obj.image,-obj.height/2,-obj.width/2);
    	}
    	ctx.restore();

        if(obj.hoverrotate){
        	hovering = true;
        	
        	ctx.beginPath();
        	ctx.fillStyle = "red";
        	ctx.rect(obj.x + obj.width-20, obj.y + obj.height-20,20,20);
        	ctx.fill();
        }
    }
    
    // draw image order
    // hitta första
    var startarrow = findArrowByStartImg("start");
    if(startarrow){
	    // följ pilarna och sätt nummer
	    var currentarrow = startarrow;
	    var num = 1;
	    while(currentarrow){
	    	if(currentarrow.toImage == null || currentarrow.fromImage == currentarrow.toImage){
	    		break;
	    	}
	    	var toimg = imagesOnCanvas[currentarrow.toImage];
	    	
	    	if(toimg.name == "mol" || toimg.name == "start"){
	    		break;
	    	}
	    	// draw box
	    	ctx.beginPath();
	    	ctx.lineWidth = 2;
	    	ctx.fillStyle = "white";
	    	ctx.strokeStyle = "#000";
	    	ctx.rect(toimg.x-6, toimg.y +toimg.height-5,22,22)
	    	ctx.fill();
	    	ctx.stroke();
	    	ctx.closePath();
	    	// draw number
	    	ctx.beginPath();
	    	ctx.fillStyle = "#000";
	    	ctx.font="bold 18px Comic Sans MS";
	    	ctx.fillText(num, toimg.x, toimg.y +toimg.height + 12);
	    	ctx.closePath();
	    	currentarrow = findArrowByStartImg(toimg.name);
	    	num++;
	    }
	}
    // change cursor
    if(hovering){
    	canvas.style.cursor="pointer";
    }else{
    	canvas.style.cursor="default";
    }
}

function findArrowByStartImg(imgname){
	var found;
	for(var x = 0, len = arrowsOnCanvas.length; x < len; x++){
    	if(imagesOnCanvas[arrowsOnCanvas[x].fromImage].name == imgname){
    		found = arrowsOnCanvas[x];
    	}
    }
	
	return found;
}

function drawArrow(ctx, fromx, fromy, tox, toy){
    //variables to be used when creating the arrow
    var headlen = 10;  
    var angle = Math.atan2(toy-fromy,tox-fromx);
    
    //starting path of the arrow from the start square to the end square and drawing the stroke
    ctx.beginPath();
    ctx.lineWidth = 2;
    ctx.moveTo(fromx, fromy);
    ctx.lineTo(tox, toy);
    ctx.strokeStyle = "#000";
    
    ctx.stroke();

    //starting a new path from the head of the arrow to one of the sides of the point
    ctx.beginPath();
    ctx.moveTo(tox, toy);
    ctx.lineTo(tox-headlen*Math.cos(angle-Math.PI/7),toy-headlen*Math.sin(angle-Math.PI/7));

    //path from the side point of the arrow, to the other side point
    ctx.lineTo(tox-headlen*Math.cos(angle+Math.PI/7),toy-headlen*Math.sin(angle+Math.PI/7));

    //path from the side point back to the tip of the arrow, and then again to the opposite side point
    ctx.lineTo(tox, toy);
    ctx.lineTo(tox-headlen*Math.cos(angle-Math.PI/7),toy-headlen*Math.sin(angle-Math.PI/7));

    //draws the paths created above
    ctx.strokeStyle = "#000";
    ctx.lineWidth = 2;
    ctx.stroke();
    ctx.fillStyle = "#8B008B";//"#000";
    ctx.fill();
    ctx.closePath();
} 


function rotateImage(context, obj) {  
    context.save();                
    context.translate(obj.width,obj.height);
    context.rotate(obj.angle);             
    context.restore();
}

requestAnimationFrame(renderScene);

window.addEventListener("load", eventListner,false);

function eventListner(){
	var canvas = document.getElementById('canvas');
	
	canvas.ondblclick = function(e) {
		if(!showarrows){
			return;
		}
		// remove the rotate listener on the images.
		canvas.onmousemove = null;
		canvas.onmousedown = null;
		for(var i = 0,len = imagesOnCanvas.length; i < len; i++) {
	        imagesOnCanvas[i].hoverrotate = false;            
	    }
		var downX = e.offsetX,downY = e.offsetY;
	    var hit = hitImage(downX,downY);
	    var obj = hit.obj;
	    if(obj != null){
	    	startDrawArrow(addArrow(hit), obj,
	    			obj.x + obj.width/2,obj.y + obj.height/2);
	    }
	}
	canvas.onmousemove = listenHover;
	canvas.onmousedown = listenMoveImage;
};

var listenHover = function(e){
	var x = e.offsetX,y = e.offsetY;
	var hit = hitImage(x,y);
	if(hit != null && hit.obj != null){
		hit.obj.hoverrotate = true;
    }else{
    	for(var i = 0,len = imagesOnCanvas.length; i < len; i++) {
            imagesOnCanvas[i].hoverrotate = false;            
        }
    }
}

var listenMoveImage = function(e){
    var downX = e.offsetX,downY = e.offsetY;
    var hit = hitImage(downX,downY);
    if(hit != null && hit.obj != null){
    	if(hitRotate(downX,downY,hit.obj)){
    		ang = hit.obj.angle + (Math.PI/2);
    		hit.obj.angle = ang;
    		var wid = hit.obj.width;
    		hit.obj.width = hit.obj.height;
    		hit.obj.height = wid;
    		hit.obj.x = hit.obj.x + ((hit.obj.height - hit.obj.width)/2);
    		hit.obj.y = hit.obj.y + ((hit.obj.width - hit.obj.height)/2);

    		if(hit.obj.angle >= Math.PI*2){
    			hit.obj.angle = 0;
    		}
    	}else{
    		startMove(hit,downX,downY);
    	}
    }
};

function addArrow(hit){
	var obj = hit.obj;
	arrowsOnCanvas.push({
		fromImage : hit.index,
		toImage : hit.index,
		x : obj.x,
		y : obj.y,
		tox : obj.x,
		toy : obj.y})
	return arrowsOnCanvas.length-1;
};

function hitRotate(x,y, obj){
	if((x > obj.x + obj.width - 20 &&
	        x < obj.x + obj.width &&
	        y > obj.y + obj.height -20 &&
	        y < obj.y + obj.height)){
		return true;
	}
	return false;
}

function hitImage(downX,downY){
	for(var x = 0,len = imagesOnCanvas.length; x < len; x++) {
        var obj = imagesOnCanvas[x];
        if(isPointInRange(downX,downY,obj)) {
        	return {obj : obj, index : x};
        	break;
        }
    }
	return null;
};

function startDrawArrow(arrowIndex, obj,downX,downY) {
	var arrow = arrowsOnCanvas[arrowIndex];
    var canvas = document.getElementById('canvas');

    arrow.x = downX;
    arrow.y = downY;
    
    canvas.onmousemove = function(e){
    	var moveX = e.offsetX, moveY = e.offsetY;
        var diffX = moveX-downX, diffY = moveY-downY;
        arrow.tox = downX+diffX;
        arrow.toy = downY+diffY;
    }
    
    canvas.onmousedown = function(e) {
    	// stop moving
    	canvas.onmousemove = function(){};
    	
    	var hit = hitImage(e.offsetX, e.offsetY);
    	
    	//check if hit another image
    	if(hit && hit.index !== arrow.fromImage){
    		placeArrow(arrow, obj, hit);
    	}else{
    		arrowsOnCanvas.pop();
    	}
        canvas.onmousedown = listenMoveImage;
        updateState();
    }
};

function placeArrow(arrow, obj1, hit){
	var obj2 = hit.obj;
	arrow.toImage = hit.index;
	
	var diffX = obj1.x - obj2.x;
	var diffY = obj1.y - obj2.y;

	var angle = Math.atan2(diffX,diffY);
	
	if(angle >= (Math.PI*3)/4 || angle < -(Math.PI*3)/4){
		arrow.tox = obj2.x + obj2.width/2;
		arrow.toy = obj2.y -1;
	}else if(angle >= Math.PI/4 ){
		arrow.tox = obj2.x + obj2.width +1;
		arrow.toy = obj2.y + (obj2.height/2);
	}else if(angle < -Math.PI/4){
		arrow.tox = obj2.x - 1;
		arrow.toy = obj2.y + (obj2.height/2);
	}else {//if(angle < Math.PI/4){
		arrow.tox = obj2.x + (obj2.width/2);
		arrow.toy = obj2.y + obj2.height+1;
	}
	
};

function startMove(hit,downX,downY) {
    var canvas = document.getElementById('canvas');
    var obj = hit.obj;
    var origX = obj.x, origY = obj.y;
    
    canvas.onmousemove = function(e) {
        var moveX = e.offsetX, moveY = e.offsetY;
        var diffX = moveX-downX, diffY = moveY-downY;

        obj.x = origX+diffX;
        obj.y = origY+diffY;
        moveArrow(hit);
        if(moveX <= 0 ||
        		moveX >= canvas.width ||
        		moveY <= 0 ||
        		moveY >= canvas.height){
        	imagesOnCanvas.splice(hit.index,1);
			removeArrows(hit.index);
        }
    }

    canvas.onmouseup = function(e) {
    	if(isOutSideCanvas(obj, canvas)){
    		if(hit.index > -1){
    			imagesOnCanvas.splice(hit.index,1);
    			removeArrows(hit.index);
    		}
    	}
    	// if gridsnap
    	if(gridsnap){
    		obj.x = (Math.round((obj.x + obj.width/2) /50)*50) - obj.width/2;
        	obj.y = (Math.round((obj.y + obj.height/2)/50)*50) - obj.height/2;
    	}
    	
    	moveArrow(hit);
    	// stop moving
    	canvas.onmousemove = listenHover;
    	canvas.onmouseup = null;
    	//update the state
    	updateState();
    }
    
};

function updateState(){

	saveData(JSON.stringify(getFilterdData()),JSON.stringify(getFilterdArrowData()));
};

function removeArrows(index){
	for (var i = 0; i < arrowsOnCanvas.length; i++) {
		var arrow = arrowsOnCanvas[i];
		if(arrow.fromImage === index || arrow.toImage === index){
			arrowsOnCanvas.splice(i,1);
			i--;
		} else {
			if(arrow.fromImage > index){
				arrowsOnCanvas[i].fromImage --; 
			} 
			if(arrow.toImage > index){
				arrowsOnCanvas[i].toImage --; 
			}
		}
	}
};

function moveArrow(hit){
	for (var x = 0; x < arrowsOnCanvas.length; x++) {
		var arrow = arrowsOnCanvas[x];
		if(arrow.toImage === hit.index){
			var objfrom = imagesOnCanvas[arrow.fromImage];
			placeArrow(arrow, objfrom, hit);
		}else if(arrow.fromImage === hit.index){
			var objfrom = imagesOnCanvas[arrow.fromImage];
			var objto = imagesOnCanvas[arrow.toImage];
			var inversHit = {obj : objto , index : arrow.toImage};
			
			arrow.x = objfrom.x + objfrom.width/2 ;
			arrow.y = objfrom.y + objfrom.height/2;
			placeArrow(arrow, objfrom, inversHit);
		}
	}
};

function isOutSideCanvas(obj, canvas){
	if(obj.x < 0 || obj.y < 0 || 
			obj.x + obj.width > canvas.width || 
			obj.y + obj.height > canvas.height){
		return true;
	}else{
		return false;
	}
	
};

function isPointInRange(x,y,obj) {
    return !(x < obj.x ||
        x > obj.x + obj.width ||
        y < obj.y ||
        y > obj.y + obj.height);
};


function allowDrop(e)
{
    e.preventDefault();
};

function drag(e)
{
    //store the position of the mouse relativly to the image position
    e.dataTransfer.setData("mouse_position_x",e.clientX - e.target.offsetLeft );
    e.dataTransfer.setData("mouse_position_y",e.clientY - e.target.offsetTop  );

    e.dataTransfer.setData("image_id",e.target.id);
};

function drop(e)
{
    e.preventDefault();
    var name = e.dataTransfer.getData("image_id");
    var image = document.getElementById( name );

    var mouse_position_x = e.dataTransfer.getData("mouse_position_x");
    var mouse_position_y = e.dataTransfer.getData("mouse_position_y");

    var canvas = document.getElementById('canvas');
    var ctx = canvas.getContext('2d');
    
    var xposs = e.clientX - canvas.offsetLeft - mouse_position_x;
    var yposs = e.clientY - canvas.offsetTop - mouse_position_y;
    
    xposs = (Math.round((xposs + image.offsetWidth/2) /50)*50) - image.offsetWidth/2;
    yposs = (Math.round((yposs + image.offsetHeight/2) /50)*50) - image.offsetHeight/2;
    
    imagesOnCanvas.push({ 
      name: name,
      image: image,  
      x:xposs,
      y:yposs,
      width: image.offsetWidth,
      height: image.offsetHeight,
      angle: 0,
  	  hoverrotate: false
    });
    updateState();
};

function getFilterdData(){
	var result = [];
	for(var x = 0,len = imagesOnCanvas.length; x < len; x++) {
        var obj = imagesOnCanvas[x];
        result.push({
        	id:		obj.id,
        	name:		obj.name, 
        	x:		obj.x,
        	y: 		obj.y,
        	width:	obj.width,
        	height: obj.height,
        	angle:  obj.angle
        });
    }
	return result;
};


function getFilterdArrowData(){
	var result = [];
	for(var x = 0,len = arrowsOnCanvas.length; x < len; x++) {
        var obj = arrowsOnCanvas[x];
        result.push({
        	id:		obj.id,
        	fromImage:	obj.fromImage,
        	toImage:	obj.toImage,
        	x:		obj.x,
        	y: 		obj.y,
        	tox:	obj.tox,
        	toy: 	obj.toy
        });
    }
	return arrowsOnCanvas;
};

function convertCanvasToImage() {
    var canvas = document.getElementById('canvas');

    var image_src = canvas.toDataURL("image/png");
    window.open(image_src);
    
};

function loadData(xhr, status, args){
	var imgs = eval('(' + args.images + ')');
	var arws = eval('(' + args.arrows + ')');
	var saved = eval('(' + args.saved + ')');
	//clear current canvas
	if(saved){
		imagesOnCanvas = [];
		arrowsOnCanvas = [];
		// add loaded data
		for(var x = 0, len = imgs.length; len > x ; x ++){
			var img = imgs[x];
			// Datagrids pageindicator dose not load all images :(
			var image = new Image(img.width,img.height);
			if(img.name === "mol" || img.name === "start" || img.name < 200){
				image.src = '../../resources/rallylydn_nyborjarklass/'+ img.name + '.png';
			}else if(img.name > 200 && img.name < 300){
				image.src = '../../resources/rallylydn_fortsattningsklass/'+ img.name + '.png';
			}else if(img.name > 300 && img.name < 400){
				image.src = '../../resources/rallylydn_avanceradklass/'+ img.name + '.png';
			}else if(img.name > 400 ){
				image.src = '../../resources/rallylydn_mastarklass/'+ img.name + '.png';
			}
			imagesOnCanvas.push({
				  id:	img.id,
			      name: img.name,
			      image: image,  
			      x: img.x,
			      y: img.y,
			      width: img.width,
			      height: img.height,
			      angle: img.angle,
		          hoverrotate: false
			    });
		}
		for (var x = 0; x < arws.length; x++) {
			var arw = arws[x];
			arrowsOnCanvas.push({
				id:	arw.id,
				fromImage:	arw.fromImage,
	        	toImage:	arw.toImage,
	        	x:		arw.x,
	        	y: 		arw.y,
	        	tox:	arw.tox,
	        	toy: 	arw.toy
			});
		}
	}
};
