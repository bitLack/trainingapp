var socket = null;
function initialize() {
 //draw background
 var canvas = document.getElementById("board");
 var ctx = canvas.getContext("2d");
 var img = document.getElementById("background_img");
 ctx.drawImage(img, 0, 0);

}

// Drag and drop functionality
function drag(ev) {
 var bounds = ev.target.getBoundingClientRect();
 var draggedSticker = {
  sticker: ev.target.getAttribute("data-sticker"),
  offsetX: ev.clientX - bounds.left,
  offsetY: ev.clientY - bounds.top
 };
 var draggedText = JSON.stringify(draggedSticker);
 ev.dataTransfer.setData("text", draggedText);
}

function drop(ev) {
 ev.preventDefault();
 var bounds = document.getElementById("board").getBoundingClientRect();
 var draggedText = ev.dataTransfer.getData("text");
 var draggedSticker = JSON.parse(draggedText);
 var stickerToSend = {
  action: "add",
  x: ev.clientX - draggedSticker.offsetX - bounds.left,
  y: ev.clientY - draggedSticker.offsetY - bounds.top,
  sticker: draggedSticker.sticker
 };
 //socket.send(JSON.stringify(stickerToSend));
}

function allowDrop(ev) {
 ev.preventDefault();
}

// Initialize on load
window.onload = initialize;
