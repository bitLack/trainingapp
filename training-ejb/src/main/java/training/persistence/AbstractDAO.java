package training.persistence;


import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;

/**
 * A container for entities, base class for OrderBook, ProductCatalogue,
 * CustomerRegistry The fundamental common operations are here 
 * (Create, Read, Update, Delete = CRUD).
 *
 * T is type for items in container K is type of id (primary key)
 *
 * @param <T> Any type
 * @param <K> Key
 */
public abstract class AbstractDAO<T, K>
        implements IDAO<T, K> {
    
    private final Class<T> clazz;
    
    protected abstract EntityManager getEntityManager();
    
    public AbstractDAO(Class<T> clazz){
        this.clazz = clazz;
    }
    
    @Override
    public void create(T t) {
    	try {
    		if(t != null){
                getEntityManager().persist(t);
            }
		} catch (Exception e) {
			e.printStackTrace();
			System.err.println("Hibernarte doing something strange.");
		}
        
    }

    @Override
    public void delete(K id) {
        T t = find(id);
        if (t != null) {
            getEntityManager().remove(t);
        }
    }

    @Override
    public T update(T t) {
       return getEntityManager().merge(t);
    }

    @Override
    public T find(K id) {
        return getEntityManager().find(clazz, id);
    }

    @Override
    public ArrayList<T> findAll() {
        return get(true, -1, -1);
    }

    @Override
    public List<T> findRange(int first, int n) {
        return get(false, first, n);
    }

    @Override
    public int count() {
        return findAll().size();
        /*
        EntityManager em = getEntityManager();
        TypedQuery<Integer> count = em.createQuery("SELECT COUNT(t) FROM " + clazz.getSimpleName() + " t", Integer.class);
        return count.getFirstResult();
        */
    }

    private ArrayList<T> get(boolean all, int first, int n) {
        EntityManager em = getEntityManager();
        ArrayList<T> found = new ArrayList<>();
        TypedQuery<T> q = em.createQuery("SELECT t FROM " + clazz.getSimpleName() + " t", clazz);
        if (!all) {
            q.setFirstResult(first);
            q.setMaxResults(n);
        }
        found.addAll(q.getResultList());
        return found;
    }
}
