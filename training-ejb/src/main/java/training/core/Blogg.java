package training.core;

import javax.ejb.EJB;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;

import training.core.dao.AccountDAO;
import training.core.dao.BloggpostDAO;
import training.core.dao.CanvasArrowDAO;
import training.core.dao.CanvasImageDAO;
import training.core.dao.CanvasMapDAO;
import training.core.dao.FeedbackDAO;
import training.core.dao.PasswordResetTokenDAO;


@Named
@ApplicationScoped
public class Blogg {
	
	@EJB
	private BloggpostDAO bloggpostDAO;
	@EJB
	private AccountDAO accountDAO;
	@EJB
	private CanvasImageDAO canvasImageDAO;
	@EJB
	private CanvasMapDAO canvasMapDAO;
	@EJB
	private CanvasArrowDAO canvasArrowDAO;
	@EJB
	private FeedbackDAO feedbackDAO;
	@EJB
	private PasswordResetTokenDAO passwordResetTokenDAO;
	
	/**
	 * @return the canvasArrowDAO
	 */
	public CanvasArrowDAO getCanvasArrowDAO() {
		return canvasArrowDAO;
	}

	/**
	 * @return the canvasMapDAO
	 */
	public CanvasMapDAO getCanvasMapDAO() {
		return canvasMapDAO;
	}

	/**
	 * @return the canvasImageDAO
	 */
	public CanvasImageDAO getCanvasImageDAO() {
		return canvasImageDAO;
	}

	/**
	 * @return the bloggpostDAO
	 */
	public BloggpostDAO getBloggpostDAO() {
		return bloggpostDAO;
	}
	
	/**
	 * @return the accountDAO
	 */
	public AccountDAO getAccountDAO() {
		return accountDAO;
	}

	/**
	 * @return the feedbackDAO
	 */
	public FeedbackDAO getFeedbackDAO() {
		return feedbackDAO;
	}

	/**
	 * @return the passwordResetTokenDAO
	 */
	public PasswordResetTokenDAO getPasswordResetTokenDAO() {
		return passwordResetTokenDAO;
	}
	
}
