package training.core.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import training.core.entity.CanvasImage;
import training.persistence.AbstractDAO;

@Stateless
public class CanvasImageDAO extends AbstractDAO<CanvasImage, Long>{
	@PersistenceContext
	private EntityManager em;
	
	public CanvasImageDAO() {
		super(CanvasImage.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}

}
