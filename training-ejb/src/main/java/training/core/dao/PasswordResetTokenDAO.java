package training.core.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import training.core.entity.PasswordResetToken;
import training.persistence.AbstractDAO;

@Stateless
public class PasswordResetTokenDAO extends AbstractDAO<PasswordResetToken, Long>{

	public PasswordResetTokenDAO() {
		super(PasswordResetToken.class);
	}

	@PersistenceContext
	private EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public PasswordResetToken getByEmail(String email){
        TypedQuery<PasswordResetToken> q = em.createQuery("SELECT a FROM passwordresettoken a WHERE a.acc.email = :email", PasswordResetToken.class)
                .setParameter("email", email);
        return q.getSingleResult();
    }
    
    public PasswordResetToken getByToken(String token){
    	TypedQuery<PasswordResetToken> q = em.createQuery("SELECT a FROM passwordresettoken a WHERE a.token = :token", PasswordResetToken.class)
                .setParameter("token", token);
    	return q.getSingleResult();
    }

}
