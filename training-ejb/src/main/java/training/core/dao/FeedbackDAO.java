package training.core.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import training.core.entity.Feedback;
import training.persistence.AbstractDAO;

@Stateless
public class FeedbackDAO extends AbstractDAO<Feedback, Long>{

	@PersistenceContext
	private EntityManager em;
	
	public FeedbackDAO(){
		super(Feedback.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public List<Feedback> getByTopic(String topic){
		List<Feedback> found = new ArrayList<>();
		TypedQuery<Feedback> q = em.createQuery("SELECT f FROM feedback f WHERE f.topic = :topic", Feedback.class)
                .setParameter("topic", topic);
        found.addAll(q.getResultList());
		return found;
	}
	
	public List<Feedback> getByAccount(String authour){
		List<Feedback> found = new ArrayList<>();
		TypedQuery<Feedback> q = em.createQuery("SELECT f FROM feedback f WHERE f.authour = :authour", Feedback.class)
                .setParameter("authour", authour);
        found.addAll(q.getResultList());
		return found;
	}
}
