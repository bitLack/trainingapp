package training.core.dao;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import training.core.entity.CanvasMap;
import training.persistence.AbstractDAO;

@Stateless
public class CanvasMapDAO extends AbstractDAO<CanvasMap, Long>{
	@PersistenceContext
	private EntityManager em;

	public CanvasMapDAO() {
		super(CanvasMap.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public ArrayList<CanvasMap> findAuthorized(String email){
		ArrayList<CanvasMap> found = new ArrayList<>();
        TypedQuery<CanvasMap> q = em.createQuery("SELECT c FROM canvasmap c WHERE c.authour.email = :email", CanvasMap.class)
                .setParameter("email", email);
        found.addAll(q.getResultList());
        return found;
	}

}
