package training.core.dao;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import training.core.entity.Account;
import training.persistence.AbstractDAO;

@Stateless
public class AccountDAO extends AbstractDAO<Account, Long>{

	public AccountDAO() {
		super(Account.class);
	}

	@PersistenceContext
	private EntityManager em;
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	

    public List<Account> getByName(String name){
        List<Account> found = new ArrayList<>();
        TypedQuery<Account> q = em.createQuery("SELECT a FROM account a WHERE a.name = :name", Account.class)
                .setParameter("name", name);
        found.addAll(q.getResultList());
        return found;
    }
    

    public List<Account> getByEmail(String email){
        List<Account> found = new ArrayList<>();
        TypedQuery<Account> q = em.createQuery("SELECT a FROM account a WHERE a.email = :email", Account.class)
                .setParameter("email", email);
        found.addAll(q.getResultList());
        return found;
    }
    
    public Account geyByActivationKey(String key){
    	TypedQuery<Account> q = em.createQuery("SELECT a FROM account a WHERE a.activationkey = :key", Account.class)
                .setParameter("key", key);
        return q.getSingleResult();
    }

}
