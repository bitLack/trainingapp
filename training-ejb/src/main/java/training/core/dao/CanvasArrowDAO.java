package training.core.dao;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import training.core.entity.CanvasArrow;
import training.persistence.AbstractDAO;

@Stateless
public class CanvasArrowDAO extends AbstractDAO<CanvasArrow, Long>{

	@PersistenceContext
	private EntityManager em;
	
	public CanvasArrowDAO() {
		super(CanvasArrow.class);
	}

	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
}
