package training.core.dao;

import java.util.ArrayList;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import training.core.entity.Bloggpost;
import training.persistence.AbstractDAO;


@Stateless
public class BloggpostDAO extends AbstractDAO<Bloggpost, Long>{

	@PersistenceContext
	private EntityManager em;

	public BloggpostDAO() {
		super(Bloggpost.class);
	}
	
	@Override
	protected EntityManager getEntityManager() {
		return em;
	}
	
	public ArrayList<Bloggpost> findAuthorized(String email){
		ArrayList<Bloggpost> found = new ArrayList<>();
        TypedQuery<Bloggpost> q = em.createQuery("SELECT c FROM bloggpost c WHERE c.authour.email = :email", Bloggpost.class)
                .setParameter("email", email);
        found.addAll(q.getResultList());
        return found;
	}

}
