package training.core.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity(name="canvasarrow")
public class CanvasArrow implements Serializable{
	private static final long serialVersionUID = 1L;
	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	private int fromImage;
	@Column(nullable = false)
	private int toImage;
	@Column(nullable = false)
	private double x;
	@Column(nullable = false)
	private double y;
	@Column(nullable = false)
	private double tox;
	@Column(nullable = false)
	private double toy;
	
	
	public CanvasArrow() {
		super();
	}
	
	public CanvasArrow(int fromImage, int toImage, double x, double y,
			double tox, double toy) {
		super();
		this.fromImage = fromImage;
		this.toImage = toImage;
		this.x = x;
		this.y = y;
		this.tox = tox;
		this.toy = toy;
	}

	public int getFromImage() {
		return fromImage;
	}

	public void setFromImage(int from) {
		this.fromImage = from;
	}

	public int getToImage() {
		return toImage;
	}

	public void setToImage(int toImage) {
		this.toImage = toImage;
	}

	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getTox() {
		return tox;
	}

	public void setTox(double tox) {
		this.tox = tox;
	}

	public double getToy() {
		return toy;
	}

	public void setToy(double toy) {
		this.toy = toy;
	}

	public long getId() {
		return id;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fromImage;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + toImage;
		long temp;
		temp = Double.doubleToLongBits(tox);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(toy);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(x);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		temp = Double.doubleToLongBits(y);
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CanvasArrow other = (CanvasArrow) obj;
		if (fromImage != other.fromImage)
			return false;
		if (id != other.id)
			return false;
		if (toImage != other.toImage)
			return false;
		if (Double.doubleToLongBits(tox) != Double.doubleToLongBits(other.tox))
			return false;
		if (Double.doubleToLongBits(toy) != Double.doubleToLongBits(other.toy))
			return false;
		if (Double.doubleToLongBits(x) != Double.doubleToLongBits(other.x))
			return false;
		if (Double.doubleToLongBits(y) != Double.doubleToLongBits(other.y))
			return false;
		return true;
	}

	
}
