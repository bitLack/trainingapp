package training.core.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 * Entity implementation class for Entity: bloggpost
 *
 */
@Entity(name="bloggpost")
public class Bloggpost implements Serializable {
	private static final long serialVersionUID = 1L;
	@Lob
	private String content;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date changeDate;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date createDate;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Account authour;
	
	@Column(nullable = false)
	private boolean publicview;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
	
	public Bloggpost() {
		super();
		Calendar cal = Calendar.getInstance();
		createDate = cal.getTime();
		changeDate = cal.getTime();
	}
	
	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}
	
	public Long getId(){
        return id;
    }

	public Account getAuthour() {
		return authour;
	}

	public void setAuthour(Account authour) {
		this.authour = authour;
	}

	/**
	 * @return the publicview
	 */
	public boolean isPublicview() {
		return publicview;
	}

	/**
	 * @param publicview the publicview to set
	 */
	public void setPublicview(boolean publicview) {
		this.publicview = publicview;
	}

	/**
	 * @return the changeDate
	 */
	public Date getChangeDate() {
		return changeDate;
	}

	/**
	 * @param changeDate the changeDate to set
	 */
	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((authour == null) ? 0 : authour.hashCode());
		result = prime * result
				+ ((changeDate == null) ? 0 : changeDate.hashCode());
		result = prime * result + ((content == null) ? 0 : content.hashCode());
		result = prime * result
				+ ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + (publicview ? 1231 : 1237);
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bloggpost other = (Bloggpost) obj;
		if (authour == null) {
			if (other.authour != null)
				return false;
		} else if (!authour.equals(other.authour))
			return false;
		if (changeDate == null) {
			if (other.changeDate != null)
				return false;
		} else if (!changeDate.equals(other.changeDate))
			return false;
		if (content == null) {
			if (other.content != null)
				return false;
		} else if (!content.equals(other.content))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (publicview != other.publicview)
			return false;
		return true;
	}


}
