package training.core.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity(name="canvasmap")
public class CanvasMap implements Serializable{
	private static final long serialVersionUID = 1L;

	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
	private long id;
	
	@Column(nullable = false)
	private String name;
	@Column(nullable = false)
	private String level;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private List<CanvasImage> canvasImages;
	
	@OneToMany(fetch = FetchType.EAGER, cascade=CascadeType.ALL)
	private List<CanvasArrow> canvasArrows;
	
	@ManyToOne(fetch = FetchType.EAGER)
	private Account authour;
	
	@Column(nullable = false)
	private String publicview;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date changeDate;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date createDate;
	
	@Column(nullable = false)
	private int width;
	
	@Column(nullable = false)
	private int height;
	
	@Column(nullable = false)
	private String arrowColor;
	
	@Column(nullable = false)
	private Boolean gridsnap;
	
	@Column(nullable = false)
	private Boolean showOrd;
	
	@Column(nullable = false)
	private Boolean showArrows;
		
	
	public CanvasMap(){
		super();
		level = "";
		publicview = "false";
		canvasImages = new ArrayList<CanvasImage>();
		canvasArrows = new ArrayList<CanvasArrow>();
		createDate = new Date(); 
		changeDate = new Date(); 
		width = 1000;
		height = 700;
		arrowColor = "purple";
		gridsnap = true;
		showOrd = true;
		showArrows = true;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getLevel() {
		return level;
	}

	public void setLevel(String level) {
		this.level = level;
	}

	public List<CanvasImage> getCanvasImages() {
		return canvasImages;
	}

	public void setCanvasImages(List<CanvasImage> canvasImages) {
		this.canvasImages = canvasImages;
	}

	public long getId(){
        return id;
    }

	public Account getAuthour() {
		return authour;
	}

	public void setAuthour(Account authour) {
		this.authour = authour;
	}

	public String getPublicview() {
		return publicview;
	}

	public void setPublicview(String publicview) {
		this.publicview = publicview;
	}

	public List<CanvasArrow> getCanvasArrows() {
		return canvasArrows;
	}

	public void setCanvasArrows(List<CanvasArrow> canvasArrows) {
		this.canvasArrows = canvasArrows;
	}
	
	

	/**
	 * @return the changeDate
	 */
	public Date getChangeDate() {
		return changeDate;
	}

	/**
	 * @param changeDate the changeDate to set
	 */
	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @param width the width to set
	 */
	public void setWidth(int width) {
		this.width = width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}

	/**
	 * @param height the height to set
	 */
	public void setHeight(int height) {
		this.height = height;
	}

	/**
	 * @return the arrowColor
	 */
	public String getArrowColor() {
		return arrowColor;
	}

	/**
	 * @param arrowColor the arrowColor to set
	 */
	public void setArrowColor(String arrowColor) {
		this.arrowColor = arrowColor;
	}
	
	

	/**
	 * @return the gridsnap
	 */
	public Boolean getGridsnap() {
		return gridsnap;
	}

	/**
	 * @param gridsnap the gridsnap to set
	 */
	public void setGridsnap(Boolean gridsnap) {
		this.gridsnap = gridsnap;
	}

	/**
	 * @return the showord
	 */
	public Boolean getShowOrd() {
		return showOrd;
	}

	/**
	 * @param showord the showord to set
	 */
	public void setShowOrd(Boolean showord) {
		this.showOrd = showord;
	}

	/**
	 * @return the showArrows
	 */
	public Boolean getShowArrows() {
		return showArrows;
	}

	/**
	 * @param showArrows the showArrows to set
	 */
	public void setShowArrows(Boolean showArrows) {
		this.showArrows = showArrows;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((arrowColor == null) ? 0 : arrowColor.hashCode());
		result = prime * result + ((authour == null) ? 0 : authour.hashCode());
		result = prime * result
				+ ((canvasArrows == null) ? 0 : canvasArrows.hashCode());
		result = prime * result
				+ ((canvasImages == null) ? 0 : canvasImages.hashCode());
		result = prime * result
				+ ((changeDate == null) ? 0 : changeDate.hashCode());
		result = prime * result
				+ ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result
				+ ((gridsnap == null) ? 0 : gridsnap.hashCode());
		result = prime * result + height;
		result = prime * result + (int) (id ^ (id >>> 32));
		result = prime * result + ((level == null) ? 0 : level.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
				+ ((publicview == null) ? 0 : publicview.hashCode());
		result = prime * result
				+ ((showArrows == null) ? 0 : showArrows.hashCode());
		result = prime * result + ((showOrd == null) ? 0 : showOrd.hashCode());
		result = prime * result + width;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CanvasMap other = (CanvasMap) obj;
		if (arrowColor == null) {
			if (other.arrowColor != null)
				return false;
		} else if (!arrowColor.equals(other.arrowColor))
			return false;
		if (authour == null) {
			if (other.authour != null)
				return false;
		} else if (!authour.equals(other.authour))
			return false;
		if (canvasArrows == null) {
			if (other.canvasArrows != null)
				return false;
		} else if (!canvasArrows.equals(other.canvasArrows))
			return false;
		if (canvasImages == null) {
			if (other.canvasImages != null)
				return false;
		} else if (!canvasImages.equals(other.canvasImages))
			return false;
		if (changeDate == null) {
			if (other.changeDate != null)
				return false;
		} else if (!changeDate.equals(other.changeDate))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (gridsnap == null) {
			if (other.gridsnap != null)
				return false;
		} else if (!gridsnap.equals(other.gridsnap))
			return false;
		if (height != other.height)
			return false;
		if (id != other.id)
			return false;
		if (level == null) {
			if (other.level != null)
				return false;
		} else if (!level.equals(other.level))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (publicview == null) {
			if (other.publicview != null)
				return false;
		} else if (!publicview.equals(other.publicview))
			return false;
		if (showArrows == null) {
			if (other.showArrows != null)
				return false;
		} else if (!showArrows.equals(other.showArrows))
			return false;
		if (showOrd == null) {
			if (other.showOrd != null)
				return false;
		} else if (!showOrd.equals(other.showOrd))
			return false;
		if (width != other.width)
			return false;
		return true;
	}

	
}
