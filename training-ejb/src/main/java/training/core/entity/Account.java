package training.core.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;


/**
 *
 * @author Fredrik
 */
@Entity(name="account")
public class Account implements Serializable{

	private static final long serialVersionUID = 1L;
	@Column(nullable = false, unique = true)
    private String email;
    @Column(nullable = false)
    private String password;
    @Column(nullable = false)
    private String name;
    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private Type type;
    @Column(nullable = false)
    private int enabled;
    
    @Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date changeDate;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = false)
	private Date createDate;
	
	@Temporal(TemporalType.DATE)
	@Column(nullable = true)
	private Date latestLoginDate;
	
	@Column(nullable = false)
	private int numberLogins;
	
	private String activationkey;
    
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    

    public Account() {
    	super();
    }
    
    public Account(String email, String password, String name) {
    	Calendar cal = Calendar.getInstance();
    	this.createDate = cal.getTime();
    	this.changeDate = cal.getTime();
        this.email = email;
        this.password = password;
        this.name = name;
        if(email.equals("admin@gmail.com")){
            this.type = Type.ROLE_ADMIN;
        }else{
            this.type = Type.ROLE_USER;
        }
        this.latestLoginDate = createDate;
        this.enabled = 0;
        this.numberLogins = 0;
    }

    public enum Type {
        ROLE_USER, ROLE_ADMIN;
    }
    
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Type getType() {
        return type;
    }
    
    public void setType(Type type) {
        this.type = type;
    }

    public int getEnabled() {
        return enabled;
    }

    public void setEnabled(int enabled) {
        this.enabled = enabled;
    }    
    
    public Long getId(){
        return id;
    }
   
    @Override
    public String toString() {
        return "Account[ email=" + email + " ]";
    }

	/**
	 * @return the changeDate
	 */
	public Date getChangeDate() {
		return changeDate;
	}

	/**
	 * @param changeDate the changeDate to set
	 */
	public void setChangeDate(Date changeDate) {
		this.changeDate = changeDate;
	}

	/**
	 * @return the createDate
	 */
	public Date getCreateDate() {
		return createDate;
	}

	/**
	 * @param createDate the createDate to set
	 */
	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	/**
	 * @return the activationkey
	 */
	public String getActivationkey() {
		return activationkey;
	}

	/**
	 * @param activationkey the activationkey to set
	 */
	public void setActivationkey(String activationkey) {
		this.activationkey = activationkey;
	}

	/**
	 * @return the latestLoginDate
	 */
	public Date getLatestLoginDate() {
		return latestLoginDate;
	}

	/**
	 * @param latestLoginDate the latestLoginDate to set
	 */
	public void setLatestLoginDate(Date latestLoginDate) {
		this.latestLoginDate = latestLoginDate;
	}

	/**
	 * @return the numberLogins
	 */
	public int getNumberLogins() {
		return numberLogins;
	}

	/**
	 * @param numberLogins the numberLogins to set
	 */
	public void setNumberLogins(int numberLogins) {
		this.numberLogins = numberLogins;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((activationkey == null) ? 0 : activationkey.hashCode());
		result = prime * result
				+ ((changeDate == null) ? 0 : changeDate.hashCode());
		result = prime * result
				+ ((createDate == null) ? 0 : createDate.hashCode());
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result + enabled;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((latestLoginDate == null) ? 0 : latestLoginDate.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + numberLogins;
		result = prime * result
				+ ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((type == null) ? 0 : type.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (activationkey == null) {
			if (other.activationkey != null)
				return false;
		} else if (!activationkey.equals(other.activationkey))
			return false;
		if (changeDate == null) {
			if (other.changeDate != null)
				return false;
		} else if (!changeDate.equals(other.changeDate))
			return false;
		if (createDate == null) {
			if (other.createDate != null)
				return false;
		} else if (!createDate.equals(other.createDate))
			return false;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		if (enabled != other.enabled)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (latestLoginDate == null) {
			if (other.latestLoginDate != null)
				return false;
		} else if (!latestLoginDate.equals(other.latestLoginDate))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (numberLogins != other.numberLogins)
			return false;
		if (password == null) {
			if (other.password != null)
				return false;
		} else if (!password.equals(other.password))
			return false;
		if (type != other.type)
			return false;
		return true;
	}

}
